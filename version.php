<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plug-in version and dependencies description.
 * @package   quiz_certainty
 * @copyright 2021 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version  = 2024070100;
$plugin->requires = 2018120300;
$plugin->maturity = MATURITY_STABLE;
$plugin->release = '1.4';
$plugin->component = 'quiz_certainty';

$plugin->dependencies = [
        'qbehaviour_certaintywithstudentfbdeferred' => 2024070100,
        'quiz_feedback' => 2022040800,
];
